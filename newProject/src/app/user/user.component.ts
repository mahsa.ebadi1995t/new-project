import {Component, Input, OnInit} from '@angular/core';
import {MatDialog } from '@angular/material/dialog';
import {User} from '../model/user';
import {AuthService} from '../services/auth.service';
import {UserdialogComponent} from '../user/userdialog/userdialog.component';
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  users: User [];
  result: any;
  public searchString: string;
  constructor(public dialog: MatDialog,
              private authService: AuthService) { }

  ngOnInit() {
    this.users = this.authService.getUsersFromData();
  }
  openDialog(state? , user?): void {
    const dialogRef = this.dialog.open(UserdialogComponent, {
      width: '300px',
      data:   user ?  [ user , state ] : [{}, state]
    });
    dialogRef.afterClosed().subscribe(result => {
      this.result = result;
    });
  }
  removeUser(user: User) {
    this.authService.deleteUser(user);
  }
  updateUser(user: User) {
   this.authService.updateUser(user);
   this.openDialog('update' , user);
  }

}

