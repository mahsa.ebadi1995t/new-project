import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../services/auth.service';
import {User} from '../../model/user';
@Component({
  selector: 'app-userdialog',
  templateUrl: './userdialog.component.html',
  styleUrls: ['./userdialog.component.scss']
})
export class UserdialogComponent implements OnInit {
  users: User [];
  add: boolean;
  update: boolean;
  userForm: FormGroup;
  isExist: any;
  operator: boolean;
  constructor(public dialogRef: MatDialogRef<UserdialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: User,
              private authService: AuthService) { }

  ngOnInit() {
    console.log(this.data);
    this.userForm = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.maxLength(10)]),
      mobile: new FormControl('', [Validators.required, Validators.pattern(/^((\\+91-?)|0)?[0-9]{10}$/)]),
      family: new FormControl('', [Validators.required, Validators.maxLength(10)])
    });
  }
  saveUser(users: User) {
    this.authService.addUser(users);
  }
  updateUser(users: User) {
    this.authService.updateUser(users);
    // this.userForm = new FormGroup({
    //   name: new FormControl([this.data.name], [Validators.required, Validators.maxLength(10)]),
    //   mobile: new FormControl(this.data.mobile, [Validators.required, Validators.pattern(/^((\\+91-?)|0)?[0-9]{10}$/)]),
    //   family: new FormControl(this.data.family, [Validators.required, Validators.maxLength(10)])
    // });
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  onsubmit() {
    console.log(this.userForm);
  }
}
