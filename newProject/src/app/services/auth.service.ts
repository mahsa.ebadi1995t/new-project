import { Injectable } from '@angular/core';
import {User} from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private users: User[] = []
  constructor() { }
  getUsersFromData(): User[] {
    return this.users;
  }
  getMobileByOpr(thirdMobile) {
    switch (Number(thirdMobile)) {
      case 1:
      case 9:
        return 'همراه اول ';
      case 2 :
        return 'رایتل';
      case 0:
      case 3:
        return 'ایرانسل ';
      default:
        return 'اپراتور ناشناخته';
    }
  }
  addUser(user: User) {
    user.operator = this.getMobileByOpr(user.mobile[2]);
    user.id = this.users.length + 1;
    this.users.push(user);
  }
  updateUser(user: User) {
    const index = this.users.findIndex(u => user.id === u.id);
    this.users[index] = user;
  }
  deleteUser(user: User) {
    this.users.splice(this.users.indexOf(user), 1);
  }

}
