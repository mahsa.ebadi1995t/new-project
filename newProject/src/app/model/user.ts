export interface User {
  id: number;
  name: string;
  family: string;
  mobile: string;
  operator: string;
}
